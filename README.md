# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Deployment** para conocer cómo desplegar el proyecto.


## Pre-requisitos 📋

_Para instalar el sotfware necesitaremos una maquina virtual o en su caso un ordenador con un sistema operativo de linux con la ultima version estable._

_Tambien necesitamos instalar el Visual Studio Code, en linux se llama code, y el navegador postman._

_Y lo ultimo que instalaremos sera el NodeJS, Morgan, MongoDB y un gestor de repositorio._


## Instalación 🔧


### **Visual Studio Code** 
_Para instalarlo y abrirlo, abrir el terminal y poner el siguente codigo._

```
sudo snap install --classic code
code .
```

---

### **Postman**
_Ahora instalamos en la terminal el postman y lo iniciamos, que nos permite realizar pruebas en las APIs._

```
sudo snap install postman
postman &
```

---

### **NodeJS**
_Despues instalamos el NodeJS._

```
sudo apt install npm
```

_A continuacion con npm instalamos el Node que se llama n._
```
sudo npm install -g n
```

_Y por ultimo instalamos la version mas estable de NodeJS y la comprobamos._

```
sudo apt install npm
```

_Para comprobar que el Node funciona se puede hacer una funcion simple en la terminal._

```
node
> Console.log("Hola")
// Mostraria por pantalla
Hola
// Para salir pondriamos
.exit
```

---

### **Gestor de repositorio**
_Instalamos por ultimo el gestor de repositorios y la configuramos._

```
sudo apt install git
git config --global user.name psr38
git config --global user.email psr38@gcloud.ua.es
git config --list 
// Para comprobar los datos que hemos metido.
```

_Para poder recuperar nuestro repositorio en el gestor de repositorios, ponemos lo siguiente_

```
git clone https://<usuario>@bitbucket.org/<usuario>/ejemplo.git
```

_Traemos los archivos que estan en el repositorio para sincronizarlo._

```
git fetch
git remote show
git status
git pull
```

_Y por ultimo si no tenia nada tu repositorio o ya estaba sincronizado, subiremos los archivos locales y los pondremos en remoto._

```
git add .
git commit -m "Primer envio Readme.md"
git push
```
---

### **Nodemon**

_Esta funcion nos permite tener nuestra aplicacion funcionando sin tener que reiniciar por cada cambio en el codiogo fuente, para instalarlo:_

```
npn -instal -D nodemon
```
_Despues de instalarlo vamos al package.json y en el apartado de scripts ponemos un nuevo apartado para iniciar nuestro servicio con nodemon._

```
"scripts": {
    "start": "nodemon index.js",
```

_Ahora gracias a nodemon ya no tenemos que parar el servidor cada vez que hagamos un cambio en el codigo fuente, podremos observar a traves de la terminal un texto que el servicio se reinicia automaticamente._

```
API REST CRUD ejecutándose en http://localhost:3000/api/:coleccion/:id
```

---

### **Morgan**

_La herramienta de morgan proporciona registro de peticiones y respuestas en las aplicaciones Express. Para instalarlo:_

```
npm -i -S morgan
```

---

### **MongoDB**

_MongoDB es una herramienta que nos permite tener una base de datos no estructurada y para instalarlo se hace de la siguiente manera:_

```
sudo apt update
sudo apt install -y mongodb
```

_Para iniciar la base de datos lo realizaremos mediante el siguiente comando:_

```
sudo systemctl start mongodb
```

_Finalmente tenemos que instalar las librerias correspondientes de mongodb para tabrajar con la base de datos y en nuestro caso debemos instalar la libreria de mongojs que nos simplifica el acceso a mongodb_

```
cd node/api-rest
npm i -S mongodb
npm i -S mongojs
```

---

## Ejecutando las pruebas ⚙️

_Para la ejecucion de las pruebas utilizaremos el navegador postman mencionado e instalado anteriormente._



### Analice las pruebas end-to-end 🔩

_Explica qué verifican estas pruebas y por qué_

### **Get**
_El get hace una consulta a la base de datos con los datos que tu hayas puesto anteriormente, y te lo muestra por el body como se puede ver en el ejemplo:_

```
Get http://localhost:3000/api/verdura/

[
    {
        "_id": "621cec5833f40f17eb9d51b8",
        "nombre": "lechuga"
    },
    {
        "_id": "621cecaa33f40f17eb9d51b9",
        "nombre": "alubia"
    }
]
```

### **Post**
_El post te vale para poner datos en la base de datos y te devuelve lo que has puesto y lo añade a la base de datos_

```
Post http://localhost:3000/api/fruta/

{
    "nombre": "cereza",
    "sabor": "dulce",
    "_id": "6220cf91eb1e6f0c04fd806d"
}
```

### **Put**
_El put te sirve para actualizar datos dentro de la base de datos, como el ejemplo de a continuacion:_

_Antes_
```
Put http://localhost:3000/api/fruta/6220cf91eb1e6f0c04fd806d

{
    "nombre": "cereza",
    "sabor": "dulce",
    "_id": "6220cf91eb1e6f0c04fd806d"
}
```
_Despues_
```
get http://localhost:3000/api/fruta/

{
    "nombre": "pera",
    "sabor": "dulce",
    "_id": "6220cf91eb1e6f0c04fd806d"
}
```
### **Delete**

_La funcion de delete te quita los datos de la base de datos_


## Despliegue 📦

_Agrega notas adicionales sobre cómo hacer deploy_

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [ROME](https://rometools.github.io/rome/) - Usado para generar RSS

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/t+uProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Paco Maciá** - *Trabajo Inicial* - [pmacia](https://github.com/pmacia)
* **Pablo Segarra** - *Documentación* - [psr38](https://bitbucket.org/psr38)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quiénes han participado en este proyecto. 

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.
